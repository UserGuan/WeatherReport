//index.js
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'北京',
    future:{},
    sk:{},
    today:{},
  },
  getCityId:function(event){
    this.setData({
      id:event.detail.value == '' ? '北京' : event.detail.value,
    })
    this.getResults()
  },
  getResults:function(){
    let that = this
    wx.request({
      url: 'http://v.juhe.cn/weather/index',
      data:{
        cityname:that.data.id,
        key:'99cbd8cac05318cbf4c504a96cd2e8c6',
      },
      success:function(rsp){
        console.log(rsp)
        that.setData({
          future:rsp.data.result.future,
          sk:rsp.data.result.sk,
          today:rsp.data.result.today
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getResults();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})